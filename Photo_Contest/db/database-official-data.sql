-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.5-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дъмп структура за таблица photo contest.category
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_category_id_uindex` (`category_id`),
  UNIQUE KEY `category_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица photo contest.category: ~3 rows (приблизително)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `name`) VALUES
	(1, 'Cats'),
	(2, 'Dogs'),
	(3, 'Nature'),
    (4, 'Travel'),
    (5, 'Everyday life'),
    (6, 'Celebration'),
    (7, 'People'),
    (8, 'Exotic'),
    (9, 'Technology'),
    (10, 'Sports'),
    (11, 'Food'),
    (12, 'Urban');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contest` (
                                         `contest_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `title` varchar(250) DEFAULT NULL,
                                         `description` varchar(8000) DEFAULT NULL,
                                         `cover_photo` varchar(1024) DEFAULT NULL,
                                         `phase_1_start` date DEFAULT NULL,
                                         `phase_1_end` date DEFAULT NULL,
                                         `phase_2_start` date DEFAULT NULL,
                                         `phase_2_end` date DEFAULT NULL,
                                         `category_id` int(11) DEFAULT NULL,
                                         `is_open` tinyint(1) DEFAULT 1,
                                         `is_calculated` tinyint(1) DEFAULT 0,
                                         PRIMARY KEY (`contest_id`),
                                         UNIQUE KEY `contest_contest_id_uindex` (`contest_id`),
                                         UNIQUE KEY `contest_title_uindex` (`title`),
                                         KEY `contest_category_category_id_fk` (`category_id`),
                                         CONSTRAINT `contest_category_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `contest` DISABLE KEYS */;
INSERT INTO `contest` (`contest_id`, `title`, `description`, `cover_photo`, `phase_1_start`, `phase_1_end`, `phase_2_start`, `phase_2_end`, `category_id`, `is_open`) VALUES
                                                                                                                                                                          (1, 'Cutest cat contest', 'You think your cat is cute? Of course, everyone thinks that! But does it have what it takes to be named the cutest cat on our website? Submit an entry to this contest and find out! ', 'https://images.unsplash.com/photo-1529257414772-1960b7bea4eb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80', '2022-03-01', '2022-03-10', '2022-03-11', '2022-03-20', 1, 1),
                                                                                                                                                                          (2, 'Is your dog a good boy (or girl)?', 'You think your dog is cute? Of course, everyone thinks that! But does it have what it takes to be named the cutest dog on our website? Submit an entry to this contest and find out! ', 'https://images.unsplash.com/photo-1554456854-55a089fd4cb2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80', '2022-03-01', '2022-03-10', '2022-03-11', '2022-03-20', 2, 1),
                                                                                                                                                                          (3, 'The perfect storm', '"There is no such thing as bad weather, only different kinds of good weather", John Ruskin says. And, as a photographer, you most probably agree! The more extraordinary the weather outside is, the bigger the chances of taking an amazing picture. And we are sure you love getting outside in the rain and searching for the perfect photo. It is now time to share it with us - we are looking for the perfect storm! ', 'https://images.unsplash.com/photo-1579004464832-0c014afa448c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80', '2022-04-20', '2022-04-30', '2022-05-01', '2022-05-20', 3, 1),
                                                                                                                                                                          (4, 'Programmer life', 'Developers wanted! Some people might think that programmers are not the most creative people, but we all know that it takes a lot of imagination to create an app or a website from scratch! A lot of developers are part-time photographers or have taken up photography as a hobby, so we are calling out to them. Show us your everyday life, your work and your sources of inspiration! ', 'https://images.unsplash.com/photo-1569012871812-f38ee64cd54c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80', '2022-04-19', '2022-05-02', '2022-05-03', '2022-05-25', 5, 1);


;


/*!40000 ALTER TABLE `contest` ENABLE KEYS */;

-- Дъмп структура за таблица photo contest.jury_contest
CREATE TABLE IF NOT EXISTS `jury_contest` (
  `user_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `is_invited` tinyint(1) DEFAULT 0,
  KEY `jury_contest_contest_contest_id_fk` (`contest_id`),
  KEY `jury_contest_user_user_id_fk` (`user_id`),
  CONSTRAINT `jury_contest_contest_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contest` (`contest_id`),
  CONSTRAINT `jury_contest_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Дъмп структура за таблица photo contest.photo
CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `story` varchar(1024) DEFAULT NULL,
  `photo` varchar(1024) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT 0,
  `ranking` int(11) DEFAULT 0,
  `is_blocked` tinyint(1) DEFAULT 0,
  `is_invited` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`photo_id`),
  UNIQUE KEY `photo_photo_id_uindex` (`photo_id`),
  KEY `photo_contest_contest_id_fk` (`contest_id`),
  KEY `photo_user_user_id_fk` (`user_id`),
  CONSTRAINT `photo_contest_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contest` (`contest_id`),
  CONSTRAINT `photo_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;


-- Дъмп структура за таблица photo contest.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица photo contest.roles: ~2 rows (приблизително)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Organizer'),
	(2, 'PhotoJunkie');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Дъмп структура за таблица photo contest.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
                                             `user_id` int(11) DEFAULT NULL,
                                             `role_id` int(11) DEFAULT NULL,
                                             KEY `users_roles_roles_role_id_fk` (`role_id`),
                                             KEY `users_roles_users_user_id_fk` (`user_id`),
                                             CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
                                             CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица photo contest.users_roles: ~8 rows (приблизително)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
                                                     (1, 1),
                                                     (2, 1),
                                                     (9, 1),
                                                     (3, 2),
                                                     (4, 2),
                                                     (5, 2),
                                                     (6, 2),
                                                     (7, 2),
                                                     (10, 2),
                                                     (8, 2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;


-- Дъмп структура за таблица photo contest.submission
CREATE TABLE IF NOT EXISTS `submission` (
  `submission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT 3,
  `comment` varchar(1024) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `voted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`submission_id`),
  UNIQUE KEY `submission_submission_id_uindex` (`submission_id`),
  KEY `submission_photo_photo_id_fk` (`photo_id`),
  KEY `submission_user_user_id_fk` (`user_id`),
  KEY `submission_contest_contest_id_fk` (`contest_id`),
  CONSTRAINT `submission_contest_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contest` (`contest_id`),
  CONSTRAINT `submission_photo_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`photo_id`),
  CONSTRAINT `submission_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `contest_user_invitations` (
                                                          `user_id` int(11) DEFAULT NULL,
                                                          `contest_id` int(11) DEFAULT NULL,
                                                          KEY `contest_user_invitations_contest_contest_id_fk` (`contest_id`),
                                                          KEY `contest_user_invitations_user_user_id_fk` (`user_id`),
                                                          CONSTRAINT `contest_user_invitations_contest_contest_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contest` (`contest_id`),
                                                          CONSTRAINT `contest_user_invitations_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп структура за таблица photo contest.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `story` varchar(250) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `avatar` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_user_id_uindex` (`user_id`),
  UNIQUE KEY `user_username_uindex` (`username`),
  UNIQUE KEY `user_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица photo contest.user: ~9 rows (приблизително)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `username`, `email`, `password`, `story`, `ranking`, `date`, `avatar`) VALUES
	(1, 'Martin', 'Karalev', 'm.karalev', 'm.karalev@abv.bg', 'martin123', '“The two most engaging powers of a photograph are to make new things familiar and familiar things new.”
– William Thackeray', 23, '2022-03-31', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649255028/q1zmxosetn5sb66qo2ve.jpg'),
	(2, 'Niya', 'Tsaneva', 'n.tsaneva', 'n.tsaneva@abv.bg', 'niya123', '“When you photograph people in color, you photograph their clothes. But when you photograph people in black and white, you photograph their souls!”
– Ted Grant', 90, '2022-04-06', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1650537325/rsz_dsc01881_dzrx9s.jpg'),
	(3, 'Georgi', 'Georgiev', 'g.georgiev', 'g.georgiev@abv.bg', 'georgi123', '“Photography to me is catching a moment which is passing, and which is true.”
– Jacques-Henri Lartigue', 15, '2022-04-08', 'https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1160&q=80'),
	(4, 'Victoria', 'Caulfield', 'vicky_photographer', 'victoria@abv.bg', 'victoria123', '“The camera makes you forget you’re there. It’s not like you are hiding but you forget, you are just looking so much.”
– Annie Leibovitz', 42, '2022-04-15', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1650544248/cropped_victoria_nlmgsm.jpg'),
	(5, 'Vasilena', 'Anastasova', 'ilovephotos', 'vasilena@abv.bg', 'test123', '“In the world of photography, you get to share a captured moment with other people.”
– James Wilson', 12, '2022-04-15', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(6, 'Martin', 'Martinov', 'Martin_m', 'martoo@gmail.com', '12345', '“If I could tell the story in words, I wouldn’t need to lug around a camera.”
– Lewis Hine', 20, '2022-04-19', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
    (7, 'Rosen', 'Kamenov', 'rosko', 'rosko@abv.bg', 'rosen123', '“Contrast is what makes photography interesting.”
– Conrad Hall', 77, '2022-04-16', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
    (8, 'Svetlana', 'Nikolinova', 'Svetlana', 'svetlana@abv.bg', 'svetlana123','“I think of photography like therapy.”
– Harry Gruyaert', 19, '2022-04-21', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(9, 'Lena', 'Howard', 'this_is_lena', 'lena@abv.bg', 'lena123', '“There is one thing the photograph must contain, the humanity of the moment.”
– Robert Frank', 17, '2022-04-21', 'https://images.unsplash.com/photo-1534528741775-53994a69daeb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=928&q=80'),
    (10, 'Michael', 'Howard', 'mike_takes_photos', 'mike@abv.bg', 'mike123', '"For me, the camera is a sketchbook, an instrument of intuition and spontaneity."
- Henri Cartier-Bresson', 33, '2022-04-21', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg');
