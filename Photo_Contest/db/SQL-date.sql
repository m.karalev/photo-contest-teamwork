-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.5-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дъмп на структурата на БД photo contest
CREATE DATABASE IF NOT EXISTS `photo contest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `photo contest`;

-- Дъмп данни за таблица photo contest.category: ~3 rows (приблизително)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `name`) VALUES
	(1, 'Cat'),
	(3, 'Dogs'),
	(2, 'Natural');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.contest: ~6 rows (приблизително)
/*!40000 ALTER TABLE `contest` DISABLE KEYS */;
INSERT INTO `contest` (`contest_id`, `title`, `description`, `cover_photo`, `phase_1_start`, `phase_1_end`, `phase_2_start`, `phase_2_end`, `category_id`, `is_open`) VALUES
	(1, 'Cats', 'Cat', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649367614/jcwxrenkbrzk2uih4htc.jpg', '2022-03-01', '2022-03-10', '2022-03-11', '2022-03-20', 1, 1),
	(2, 'Dogs', 'Dogs', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649367835/dy4bnt5ajjuh64a8yw01.jpg', '2022-04-01', '2022-04-07', '2022-04-08', '2022-04-30', 2, 1),
	(3, 'Nature', 'Nature', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649412814/Nature_uxsqu6.jpg', '2022-04-08', '2022-04-30', '2022-05-01', '2022-04-10', 3, 1),
	(4, 'Blue sky', 'Blue sky', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1650444007/sq7r1lrxhtvx6wrqetbg.jpg', '2022-04-20', '2022-04-30', '2022-05-01', '2022-05-08', 2, 1),
	(5, 'New contes', 'New Contest', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1650444561/zjs9bzwm4mizndcv3dyi.jpg', '2022-04-21', '2022-04-26', '2022-04-30', '2022-05-05', 2, 0),
	(6, 'Test1', 'Test1', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1650445363/qnajvrulwudnuqxuweqp.jpg', '2022-04-20', '2022-04-23', '2022-04-24', '2022-04-27', 1, 1);
/*!40000 ALTER TABLE `contest` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.jury_contest: ~5 rows (приблизително)
/*!40000 ALTER TABLE `jury_contest` DISABLE KEYS */;
INSERT INTO `jury_contest` (`user_id`, `contest_id`, `is_invited`) VALUES
	(8, 1, 1),
	(8, 2, 1),
	(8, 3, 1),
	(2, 6, 0),
	(1, 6, 0);
/*!40000 ALTER TABLE `jury_contest` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.photo: ~12 rows (приблизително)
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` (`photo_id`, `title`, `story`, `photo`, `user_id`, `contest_id`, `score`, `ranking`, `is_blocked`) VALUES
	(1, 'Cat', 'Cat', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367782/uduaomwag6pwk8jgaj7k.jpg', 1, 1, 10, 0, 0),
	(2, 'Dog', 'Dog', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367691/icp6kxfekhkfm6l96drm.jpg', 1, 1, 0, 0, 0),
	(12, 'MartinCV', 'Image for CV', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367782/uduaomwag6pwk8jgaj7k.jpg', 1, 1, 0, 0, 0),
	(13, 'MartinCV', 'Image for CV', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367643/frnlatj8cpciojmsh3hs.jpg', 1, 1, 0, 0, 0),
	(14, 'My cat', 'My cat', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367614/jcwxrenkbrzk2uih4htc.jpg', 1, 1, 0, 0, 0),
	(15, 'My cat2', 'My cat2', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367643/frnlatj8cpciojmsh3hs.jpg', 1, 1, 0, 0, 0),
	(16, 'My cat3', 'My cat3', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367661/ffsv0fajfzxen4mz3awf.jpg', 1, 1, 0, 0, 0),
	(17, 'White cat', 'White cat', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367691/icp6kxfekhkfm6l96drm.jpg', 1, 1, 0, 0, 0),
	(18, 'Curious cat', 'Curious cat', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367782/uduaomwag6pwk8jgaj7k.jpg', 1, 1, 0, 0, 0),
	(19, 'Black dog', 'Black dog', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367835/dy4bnt5ajjuh64a8yw01.jpg', 1, 2, 0, 0, 0),
	(20, 'Good dog', 'good dog', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1649367950/wwyl8eroq82gcbydlfeo.jpg', 1, 2, 0, 0, 0),
	(22, 'Proba', 'Proba', 'http://res.cloudinary.com/dqiiomdth/image/upload/v1650457620/khebfqpy8rdgbz6jtpnf.jpg', 8, 6, 0, 0, 0);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.roles: ~2 rows (приблизително)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Organizer'),
	(2, 'Ordinary');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.submission: ~16 rows (приблизително)
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
INSERT INTO `submission` (`submission_id`, `user_id`, `photo_id`, `score`, `comment`, `contest_id`, `voted`) VALUES
	(1, 1, 1, 3, 'Good', 1, 0),
	(2, 1, 2, 3, 'Very good', 2, 0),
	(4, 1, 17, 4, 'Very good', 3, 0),
	(5, 8, 2, 0, 'Comment that the category is wrong. ', 1, 0),
	(6, 8, 2, 0, 'Category is wrong.', 1, 0),
	(7, 8, 2, 0, 'Category is wrong.', 1, 0),
	(8, 8, 2, 5, 'dff', 1, 0),
	(9, 8, 2, 0, 'Category is wrong.', 1, 0),
	(10, 8, 2, 10, 'No comment from jury test', 1, 0),
	(11, 8, 2, 10, 'Very good photo!', 1, 0),
	(12, 8, 20, 6, 'No comment from jury wit username: test', 2, 0),
	(13, 8, 19, 10, 'No comment from jury wit username: test', 2, 0),
	(14, 2, 22, 3, 'Not comment', 6, 0),
	(15, 1, 22, 3, 'Not comment', 6, 0),
	(16, 1, 19, 10, 'Good', 2, 1),
	(17, 1, 20, 9, 'Yes', 2, 1);
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.user: ~9 rows (приблизително)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `username`, `email`, `password`, `story`, `ranking`, `date`, `avatar`) VALUES
	(1, 'Martin', 'Karalev', 'm.karalev', 'm.karalev@abv.bg', 'martin123', 'Story', 23, '2022-03-31', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(2, 'Ivan', 'Ivanov', 'i.ivanov', 'i.ivanov@abv.bg', 'ivan123', 'Story', 0, '2022-04-06', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(4, 'Georgi', 'Georgiev', 'g.georgorv', 'g.georgiev@abv.bg', 'georgi123', 'story', 0, '2022-04-08', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(7, 'User', 'User', 'user', 'user@abv.bg', 'user123', 'story', 0, '2022-04-15', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(8, 'Test', 'Test', 'test', 'test@abv.bg', 'test123', 'story', 0, '2022-04-15', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg'),
	(9, 'testuser', 'testuser', 'testuser', 'testuser@abv.bg', 'testuser123', NULL, 0, '2022-04-16', NULL),
	(10, 'Martin', 'Karalev', 'Martin_Karalev', 'test@gmail.com', '12345', NULL, 0, '2022-04-19', NULL),
	(11, 'Svetlana', 'Svetlana', 'Svetlana', 'svetlana@abv.bg', 'svetlana123', NULL, 0, '2022-04-21', NULL),
	(12, 'avatar', 'avatar', 'avatar', 'avatar@abv.bg', 'avatar123', 'You don\'t have a story yet.', 0, '2022-04-21', 'https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.users_roles: ~8 rows (приблизително)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(7, 2),
	(8, 2),
	(9, 2),
	(10, 2),
	(11, 2),
	(12, 2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.user_like_user: ~0 rows (приблизително)
/*!40000 ALTER TABLE `user_like_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_like_user` ENABLE KEYS */;

-- Дъмп данни за таблица photo contest.user_photo_like: ~0 rows (приблизително)
/*!40000 ALTER TABLE `user_photo_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_photo_like` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
