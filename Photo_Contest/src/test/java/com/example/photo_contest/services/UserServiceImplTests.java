package com.example.photo_contest.services;


import com.example.photo_contest.exceptions.*;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.Optional;

import static com.example.photo_contest.Helpers.createMockAdmin;
import static com.example.photo_contest.Helpers.createMockUser;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository repository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_returnUser() {

        var user = createMockUser();

        Mockito.when(repository.getAll(Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.getAll(user, Optional.empty());

        Mockito.verify(repository, Mockito.times(1))
                .getAll(Optional.empty());
    }


    @Test
    public void getByUsername_Should_returnUser_when_matchExists() {
        var user = createMockUser();
        Mockito.when(repository.getByUsername(user.getUsername()))
                .thenReturn(user);

        User result = service.getByUsername(user.getUsername());

        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getId(), result.getId()),
                () -> Assertions.assertEquals(user.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(user.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(user.getFirstName(), result.getFirstName())
        );
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(repository.getById(mockUser.getId()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockUser.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }

    @Test
    public void getByEmail_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(repository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);
        // Act
        User result = service.getByEmail(mockUser.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }


    @Test
    public void create_Should_Throw_When_UserWithSameNameExists() {
        //Arrange
        var mockUser = createMockUser();

        Mockito.when(repository.getByUsername(mockUser.getUsername())).thenReturn(mockUser);

        //Act,Assert
        Assertions.assertThrows(DuplicateUserException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_Should_Throw_When_UserWithSameEmailExists() {
        //Arrange
        var user = createMockUser();

        Mockito.when(repository.getByUsername(user.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", user.getUsername()));
        Mockito.when(repository.getByEmail(user.getEmail())).thenReturn(user);

        //Act,Assert
        Assertions.assertThrows(DuplicateEmailException.class, () -> service.create(user));
    }

    @Test
    public void create_Should_CallRepository_When_UserWIthSameNameExists() {
        //Arrange

        var user = createMockUser();

        Mockito.when(repository.getByUsername(user.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", user.getUsername()));
        Mockito.when(repository.getByEmail(user.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "email", user.getUsername()));

        service.create(user);

        Mockito.verify(repository, Mockito.times(1))
                .create(user);
    }

    @Test
    public void update_should_callRepository_when_isCorrect() {
        User user = createMockUser();

        Mockito.when(repository.getById(user.getId())).thenReturn(user);

        service.update(1, user, user);

        Mockito.verify(repository, Mockito.times(1))
                .update(user);
    }

    @Test
    public void update_should_ThrowException_when_tryToChangUsername() {
        User user = createMockUser();
        User user1 = createMockUser();
        user1.setUsername("test");

        Mockito.when(repository.getById(1)).thenReturn(user1);


        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> service.update(1, user, user1));
    }

    @Test
    public void update_should_ThrowException_when_idIsDifferent() {
        User user = createMockUser();
        User user1 = createMockUser();
        user1.setId(2);

        Mockito.when(repository.getById(1)).thenReturn(user1);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(1, user, user1));
    }

    @Test
    public void update_should_ThrowException_when_userNotExisting() {
        User user = createMockUser();
        User user1 = createMockUser();
        user1.setId(2);

        Mockito.when(repository.getById(5)).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(5, user, user1));
    }

    @Test

    public void delete_should_throwException_when_initiatorIsNotAdmin() {
        User user = createMockUser();
        Mockito.when(repository.getById(user.getId()))
                .thenReturn(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(user.getId(), user));
    }

    @Test
    public void delete_should_callRepository_when_initiatorIsAdmin() {
        User user = createMockUser();
        User admin = createMockAdmin();

        Mockito.when(repository.getById(user.getId()))
                .thenReturn(user);

        service.delete(user.getId(), admin);

        Mockito.verify(repository, Mockito.times(1))
                .delete(user.getId());
    }

    @Test
    public void filter_should_callRepository_when_initiatorIsAmin(){
        User user = createMockAdmin();
        Optional<String> firstName = Optional.ofNullable(user.getFirstName());
        Optional<String> lastName = Optional.ofNullable(user.getLastName());
        Optional<String> username = Optional.ofNullable(user.getUsername());
        Optional<String> email = Optional.ofNullable(user.getUsername());
        Optional<String> sort = Optional.of("desc");


        service.filter(user,firstName,lastName,username,email,sort );

        Mockito.verify(repository, Mockito.times(1))
                .filter(firstName,lastName,username,email,sort);
    }

}
