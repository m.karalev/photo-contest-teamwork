package com.example.photo_contest.services;

import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.repositories.contracts.SubmissionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class SubmissionServiceImplTests {

    @Mock
    SubmissionRepository repository;

    @InjectMocks
    SubmissionServiceImpl service;
    PhotoRepository photoRepository;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll(createMockAdmin());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnSubmission_when_matchExist() {
        // Arrange
        Submission mockSubmission = createMockSubmission();
        Mockito.when(repository.getById(mockSubmission.getId()))
                .thenReturn(mockSubmission);
        // Act
        Submission result = service.getById(mockSubmission.getId());

        // Assert
         Assertions.assertEquals(mockSubmission.getId(), result.getId());
    }

    @Test
    public void create_Should_CallRepository() {
        //Arrange

        Submission mockSubmission = createMockSubmission();

        service.create(mockSubmission);

        Mockito.verify(repository, Mockito.times(1))
                .create(mockSubmission);
    }

    @Test
    public void getByUserIdPhotoId_Should_CallRepository() {
        //Arrange

        Submission mockSubmission = createMockSubmission();
        User mockUser = createMockUser();
        Photo mockPhoto = createMockPhoto();

        service.getByUserIdPhotoId(mockUser.getId(), mockPhoto.getId());

        Mockito.verify(repository, Mockito.times(1))
                .getByUserIdPhotoId(mockUser.getId(), mockPhoto.getId());
    }

    @Test
    public void update_Should_ThrowWhenUserNotJury() {
        Submission mockSubmission = createMockSubmission();
        User mockUser = createMockAdmin();
        Mockito.when(repository.getByUserIdPhotoId(mockUser.getId(), mockSubmission.getPhoto().getId()))
                .thenReturn(mockSubmission);
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(mockSubmission, mockUser));

    }

    @Test
    public void update_Should_ThrowWhenUserAlreadyVoted() {
        Submission mockSubmission = createMockSubmission();
        User mockUser = createMockAdmin();
        mockSubmission.getContest().getJury().add(mockUser);
        mockSubmission.setVoted(true);
        Mockito.when(repository.getByUserIdPhotoId(mockUser.getId(), mockSubmission.getPhoto().getId()))
                .thenReturn(mockSubmission);
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockSubmission, mockUser));

    }
}
