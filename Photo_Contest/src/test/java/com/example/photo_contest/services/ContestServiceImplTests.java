package com.example.photo_contest.services;

import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.ContestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.photo_contest.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository repository;

    @InjectMocks
    ContestServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll(createMockAdmin());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getPhaseOne_should_callRepository() {
        // Arrange
        Mockito.when(repository.getPhaseOne())
                .thenReturn(new ArrayList<>());

        // Act
        service.getPhaseOne(createMockAdmin());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getPhaseOne();
    }

    @Test
    void getPhaseTwo_should_callRepository() {
        // Arrange
        Mockito.when(repository.getPhaseTwo())
                .thenReturn(new ArrayList<>());

        // Act
        service.getPhaseTwo(createMockAdmin());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getPhaseTwo();
    }

    @Test
    void getFinished_should_callRepository() {
        // Arrange
        Mockito.when(repository.getFinished())
                .thenReturn(new ArrayList<>());

        // Act
        service.getFinished(createMockAdmin());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getFinished();
    }

    @Test
    void update_should_callRepository() {
        // Arrange
        Contest mockContest = createMockContest();

        Mockito.when(repository.getByName(mockContest.getTitle())).thenReturn(mockContest);
        // Act
        service.update(mockContest);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    void getCurrent_should_callRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(repository.getCurrent(user.getId()))
                .thenReturn(new ArrayList<>());

        // Act
        service.getCurrent(user.getId());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getCurrent(user.getId());
    }

    @Test
    void getPast_should_callRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(repository.getPast(user.getId()))
                .thenReturn(new ArrayList<>());

        // Act
        service.getPast(user.getId());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getPast(user.getId());
    }

    @Test
    void getLatest_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getLatest();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getFinishedOpen_should_callRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(repository.getFinishedOpen())
                .thenReturn(new ArrayList<>());

        // Act
        service.getFinishedOpen();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getFinishedOpen();
    }


    @Test
    void getActiveOpen_should_callRepository() {
        // Arrange
        Mockito.when(repository.getActiveOpen())
                .thenReturn(new ArrayList<>());

        // Act
        service.getActiveOpen();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getActiveOpen();
    }


    @Test
    public void getPhaseOne_should_throwException_when_userIsNotAdmin() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getPhaseOne(createMockUser()));
    }

    @Test
    public void getPhaseTwo_should_throwException_when_userIsNotAdmin() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getPhaseTwo(createMockUser()));
    }

    @Test
    public void getFinished_should_throwException_when_userIsNotAdmin() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getFinished(createMockUser()));
    }

    @Test
    void getInvitationsPhaseTwo_should_callRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(repository.getPhaseTwo())
                .thenReturn(new ArrayList<>());

        // Act
        service.getInvitationsPhaseTwo(user);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getPhaseTwo();
    }

    @Test
    void getInvitationsPhaseOne_should_callRepository() {
        // Arrange
        Mockito.when(repository.getPhaseOne())
                .thenReturn(new ArrayList<>());

        // Act
        service.getInvitationsPhaseOne(createMockUser());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getPhaseOne();
    }

    @Test
    public void update_should_throwException_when_contestNameIsTaken() {
        // Arrange
        Contest mockContest = createMockContest();
        mockContest.setTitle("test-name");
        Contest anotherMockBeer = createMockContest();
        anotherMockBeer.setId(2);
        anotherMockBeer.setTitle("test-name");

   //  Mockito.when(repository.getById(Mockito.anyInt()))
   //          .thenReturn(mockContest);
   //
    Mockito.when(repository.getByName(Mockito.anyString()))
            .thenReturn(anotherMockBeer);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockContest));
    }


    @Test
    void getAll_should_throw_when_userIsNotOrganizer() {

        Assertions.assertThrows(UnauthorizedOperationException.class, () ->  service.getAll(createMockUser()));
    }


    @Test
    public void getById_should_returnContest_when_matchExist() {
        // Arrange
       Contest mockContest = createMockContest();
        Mockito.when(repository.getById(mockContest.getId()))
                .thenReturn(mockContest);
        // Act
        Contest result = service.getById(mockContest.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockContest.getId(), result.getId()),
                () -> Assertions.assertEquals(mockContest.getTitle(), result.getTitle())
        );
    }



    @Test
    public void create_should_throw_when_contestWithSameNameExists() {
        // Arrange
        Contest mockContest = createMockContest();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockContest));
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Mockito.when(repository.filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(createMockAdmin(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

  @Test
  public void filter_should_throw_when_initiatorIsNotAmin(){
      User user = createMockUser();
            Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.filter(user, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));

  }


}
