package com.example.photo_contest.services;


import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.CategoryRepository;
import com.example.photo_contest.services.contracts.CategoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.photo_contest.Helpers.createMockCategory;
import static com.example.photo_contest.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository repository;

    @InjectMocks
    CategoryServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(repository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCategory_when_matchExist() {
        // Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(repository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);
        // Act
        Category result = service.getById(mockCategory.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void create_should_throw_when_categoryWithSameNameExists() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(repository.getByName(mockCategory.getName()))
                .thenReturn(mockCategory);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCategory));
    }

    @Test
    public void create_should_callRepository_when_categoryWithSameNameDoesNotExist() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(repository.getByName(mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockCategory);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    public void update_should_callRepository_when_isCorrect() {
        Category mockCategory = createMockCategory();

        service.update(mockCategory);

        Mockito.verify(repository, Mockito.times(1))
                .update(mockCategory);
    }

}
