package com.example.photo_contest;

import com.example.photo_contest.models.*;
import com.example.photo_contest.services.SubmissionServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static User createMockUser() {
        return createMockUser("PhotoJunkie");
    }

    public static User createMockAdmin() {
        return createMockUser("Organizer");
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setRoles(Set.of(createMockRole(role)));
        return mockUser;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        return mockCategory;
    }


    public static Contest createMockContest() {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("MockTitle");
        mockContest.setDescription("MockDescription");
        mockContest.setCoverPhoto("MockCoverPhoto");
        Date start1 = new Date();
        Date end1 = new Date();
        Date start2 = new Date();
        Date end2 = new Date();
        mockContest.setPhaseOneStart(start1);
        mockContest.setPhaseOneEnd(end1);
        mockContest.setPhaseTwoStart(start2);
        mockContest.setPhaseTwoEnd(end2);
        mockContest.setCategory(createMockCategory());
        mockContest.setOpen(true);
        return mockContest;
    }

    public static Photo createMockPhoto() {
        var mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setTitle("MockTitle");
        mockPhoto.setPhoto("MockLink");
        mockPhoto.setStory("MockStory");
        mockPhoto.setUser(createMockUser("PhotoJunkie"));
        mockPhoto.setContest(createMockContest());
        return mockPhoto;
    }

    public static Submission createMockSubmission() {
        var mockSubmission = new Submission();
        mockSubmission.setId(1);
        mockSubmission.setUser(createMockUser());
        mockSubmission.setPhoto(createMockPhoto());
        mockSubmission.setScore(23);
        mockSubmission.setComment("MockComment MockComment");
        mockSubmission.setContest(createMockContest());
        mockSubmission.setVoted(false);
        mockSubmission.getContest().setJury(new HashSet<>());
        return mockSubmission;
    }

    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
