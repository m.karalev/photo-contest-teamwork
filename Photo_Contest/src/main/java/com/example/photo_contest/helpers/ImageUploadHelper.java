package com.example.photo_contest.helpers;

import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@Component
public class ImageUploadHelper {


    public static String uploadToCloudinary(MultipartFile multipartFile) throws IOException {

        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dqiiomdth",
                "api_key", "498936268698556",
                "api_secret", "FA1JFQ9ErzPpa7OuTtfk_m4oqqc"));
        if (multipartFile.isEmpty()) {
            return "https://images.unsplash.com/photo-1513031300226-c8fb12de9ade?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80";
        }
        Map upload = cloudinary.uploader().cloudinary().uploader().upload(multipartFile.getBytes(), ObjectUtils.emptyMap());
        return upload.get("url").toString();
    }
}
