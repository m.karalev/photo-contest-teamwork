package com.example.photo_contest.utils_mappers;

import com.example.photo_contest.models.*;
import com.example.photo_contest.models.dto.*;
import com.example.photo_contest.repositories.contracts.CategoryRepository;
import com.example.photo_contest.repositories.contracts.ContestRepository;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ModelMapper {

    public static final int DEFAULT_SCORE_BY_JURY = 3;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final PhotoRepository photoRepository;
    private final ContestRepository contestRepository;


    public ModelMapper(UserRepository userRepository, CategoryRepository categoryRepository, PhotoRepository photoRepository, ContestRepository contestRepository) {
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.photoRepository = photoRepository;
        this.contestRepository = contestRepository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        LocalDateTime date = LocalDateTime.now();
        dtoToObject(user, userDto, date);
        return user;
    }

    public User fromDto(UserDto userDTO, int id) {
        User user = userRepository.getById(id);
        LocalDateTime date = user.getDate();
        dtoToObject(user, userDTO, date);
        return user;
    }

    private void dtoToObject(User user, UserDto userDto, LocalDateTime date) {
        String newPassword = userDto.getPassword();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setStory(userDto.getStory());
        user.setAvatar(userDto.getAvatar());
        if (newPassword != null && !newPassword.isEmpty()) user.setPassword(newPassword);
        user.setDate(date);
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setDate(LocalDateTime.now());
        Role role = new Role();
        role.setId(2);
        role.setName("PhotoJunkie");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        user.setAvatar("https://res.cloudinary.com/dqiiomdth/image/upload/v1649372624/avatar_kdfsgk.jpg");
        user.setStory("You don't have a story yet.");
        return user;
    }

    public Contest fromDto(ContestDto contestDto) {
        Contest contest = new Contest();
        dtoToContest(contestDto, contest);
        return contest;
    }

    private void dtoToContest(ContestDto contestDto, Contest contest) {
        Category category = categoryRepository.getById(contestDto.getCategoryId());
        contest.setTitle(contestDto.getTitle());
        contest.setDescription(contestDto.getDescription());
        contest.setCoverPhoto(contestDto.getCoverPhoto());
        contest.setPhaseOneStart(Date.valueOf(contestDto.getPhaseOneStart()));
        contest.setPhaseOneEnd(Date.valueOf(contestDto.getPhaseOneEnd()));
        contest.setPhaseTwoStart(Date.valueOf(contestDto.getPhaseTwoStart()));
        contest.setPhaseTwoEnd(Date.valueOf(contestDto.getPhaseTwoEnd()));
        contest.setCategory(category);
        contest.setOpen(contestDto.getIsOpen().equals("true"));

    }

    public Photo dtoToObject(String title, String story, String link, int userId, int contestId) {
        Photo photo = new Photo();
        photo.setTitle(title);
        photo.setStory(story);
        photo.setPhoto(link);
        photo.setUser(userRepository.getById(userId));
        photo.setContest(contestRepository.getById(contestId));
        return photo;
    }

    public Photo dtoToObject(PhotoDto photoDto, User user, Contest contest, int jurySize) {
        Photo photo = new Photo();
        photo.setUser(user);
        photo.setContest(contest);
        photo.setPhoto(photoDto.getPhoto());
        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
        photo.setScore(jurySize * DEFAULT_SCORE_BY_JURY);
        return photo;
    }



}
