package com.example.photo_contest.utils_mappers;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.dto.ReviewDto;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SubmissionMapper {


    private final ContestService contestService;


    public SubmissionMapper(ContestService contestService) {
        this.contestService = contestService;
    }

    public Submission DtoToObject(ReviewDto dto, User user, Photo photo, Contest contest){

        Submission submission = new Submission();
        if (dto.getPoints()>0){
            submission.setUser(user);
            submission.setPhoto(photo);
            submission.setScore(dto.getPoints());
            if (dto.getComment().isBlank()){
                submission.setComment(String.format("No comment from jury member with username: %s", user.getUsername()));
            }else {
                submission.setComment(dto.getComment());
            }
            submission.setContest(contest);
        }else {
            submission.setUser(user);
            submission.setPhoto(photo);
            submission.setScore(dto.getPoints());
            submission.setComment("Wrong category.");
            submission.setContest(contest);
        }
        return submission;
    }

    public Submission DefaultDtoToObject( User user, Photo photo){
        Submission submission = new Submission();
        submission.setUser(user);
        submission.setPhoto(photo);
        submission.setScore(3);
        submission.setComment("No comment");
        Contest contest = contestService.getById(photo.getContest().getId());
        submission.setContest(contest);
        submission.setVoted(false);
        return submission;
    }

}
