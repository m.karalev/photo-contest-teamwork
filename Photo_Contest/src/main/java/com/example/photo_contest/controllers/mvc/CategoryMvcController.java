package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.models.User;
import com.example.photo_contest.services.contracts.CategoryService;
import com.example.photo_contest.services.contracts.UserPointCalculation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
@RequestMapping("/categories")
public class CategoryMvcController {
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;
    private final UserPointCalculation userPointCalculation;

    public CategoryMvcController(CategoryService categoryService, AuthenticationHelper authenticationHelper, UserPointCalculation userPointCalculation) {
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
        this.userPointCalculation = userPointCalculation;
    }


    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentlyLoggedInUser")
    public User getCurrentlyLoggedInUser(HttpSession session) {
        User user = null;
        if (popularIsAuthenticated(session)) user = authenticationHelper.tryGetUser(session);
        return user;
    }

    @ModelAttribute
    private void pointCalculation(){
        userPointCalculation.pointCalculation();
    }

    @GetMapping("/create-category")
    public String showNewCategoryPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("category", new Category());
        return "create-category";
    }

    @PostMapping("/create-category")
    public String createCategory(@Valid @ModelAttribute("category") Category category, BindingResult errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "create-category";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            categoryService.create(category);
            return "redirect:/dashboard";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            return "not-authorized";
        }
    }
}
