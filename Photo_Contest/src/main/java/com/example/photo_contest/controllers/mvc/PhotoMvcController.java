package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.*;
import com.example.photo_contest.models.dto.ReviewDto;
import com.example.photo_contest.services.contracts.*;
import com.example.photo_contest.utils_mappers.ModelMapper;
import com.example.photo_contest.utils_mappers.SubmissionMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Controller
@RequestMapping("/photos")
public class PhotoMvcController {
    private final UserService userService;

    private final CategoryService categoryService;
    private final PhotoService photoService;
    public final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final SubmissionService submissionService;
    private final ContestService contestService;
    private final SubmissionMapper submissionMapper;
    private final UserPointCalculation userPointCalculation;

    public PhotoMvcController(UserService userService, CategoryService categoryService, PhotoService photoService, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, SubmissionService submissionService, ContestService contestService, SubmissionMapper submissionMapper, UserPointCalculation userPointCalculation) {
        this.userService = userService;
        this.categoryService = categoryService;
        this.photoService = photoService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.submissionService = submissionService;
        this.contestService = contestService;
        this.submissionMapper = submissionMapper;
        this.userPointCalculation = userPointCalculation;
    }

    @ModelAttribute
    private void pointCalculation(){
        userPointCalculation.pointCalculation();
    }

    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (popularIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getId();
        return userId;
    }

    @ModelAttribute("currentlyLoggedInUser")
    public User getCurrentlyLoggedInUser(HttpSession session) {
        User user = null;
        if (popularIsAuthenticated(session)) user = authenticationHelper.tryGetUser(session);
        return user;
    }



    @GetMapping("/{id}")
    public String getSinglePhoto(@PathVariable int id, Model model, HttpSession session , @ModelAttribute ReviewDto review) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Photo photo = photoService.getById(id);
            Category category = photo.getContest().getCategory();
            Contest contest = photo.getContest();
            List<Submission> submissions = new ArrayList<>();
            Submission submission = new Submission();
            if (user.isOrganizer() && contest.isPhaseTwo()){
               submission = submissionService.getByUserIdPhotoId(user.getId(), photo.getId());
            }
            if (contest.isFinished()){
                submissions = submissionService.getByPhotoId(photo.getId());
            }
            model.addAttribute("review", new ReviewDto());
            model.addAttribute("user", user);
            model.addAttribute("photo", photo);
            model.addAttribute("category", category);
            model.addAttribute("contest", contest);
            model.addAttribute("submission", submission);
            model.addAttribute("submissions", submissions);
            if (user.isOrganizer() && photo.getContest().isPhaseTwo()){
                return "photos";
            }
            return "photos-user-view";

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }
    }


    @PostMapping("/review{id}")
    public String createPhotoReview(@Valid @ModelAttribute("review") ReviewDto review,
                                    @PathVariable int id,
                                    BindingResult error,
                                    HttpSession session,
                                    Model model) {
        if (error.hasErrors()) {
            return "contest-phase-two";
        }
        try {
            Photo photo = photoService.getById(id);
            User user = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(photo.getContest().getId());
            Submission submission = submissionMapper.DtoToObject(review, user, photo, contest);
            submissionService.update(submission , user);
            Set<Photo> photos = contest.getPhotos();
            model.addAttribute("contest", contest);
            model.addAttribute("photos", photos);
            model.addAttribute("user", user);
            return "contest-phase-two";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";

        }
    }
}