package com.example.photo_contest.controllers.mvc;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.AuthenticationFailureException;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.helpers.ImageUploadHelper;
import com.example.photo_contest.models.dto.FilterUserDto;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.dto.UserDto;
import com.example.photo_contest.models.enums.UserSortOption;
import com.example.photo_contest.services.contracts.UserPointCalculation;
import com.example.photo_contest.utils_mappers.ModelMapper;
import com.example.photo_contest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    public final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final UserPointCalculation userPointCalculation;


    @Autowired
    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, UserPointCalculation userPointCalculation) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.userPointCalculation = userPointCalculation;
    }

    @ModelAttribute
    private void pointCalculation(){
        userPointCalculation.pointCalculation();
    }

    @ModelAttribute("isAuthenticated")
    public boolean popularIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("userId")
    public int getUserId(HttpSession session) {
        int userId = 0;
        if (popularIsAuthenticated(session)) userId = authenticationHelper.tryGetUser(session).getId();
        return userId;
    }

    @ModelAttribute("currentlyLoggedInUser")
    public User getCurrentlyLoggedInUser(HttpSession session) {
        User user = null;
        if (popularIsAuthenticated(session)) user = authenticationHelper.tryGetUser(session);
        return user;
    }

    @ModelAttribute("sortUserOptions")
    public UserSortOption[] populatePostOptions() {
        return UserSortOption.values();
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Optional<String> search = Optional.of("");
            List<User> users = userService.getAll(user, search);
            model.addAttribute("users", users);
            model.addAttribute("filterUserDto", new FilterUserDto());
            return "users";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";

        }
    }

    @PostMapping("/filter")
    public String filterUsers(@ModelAttribute("filterUserDto") FilterUserDto filterUserDto,
                              Model model,
                              HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        List<User> filter = userService.filter(
                user,
                Optional.ofNullable(filterUserDto.getSearch().isBlank() ? null : filterUserDto.getSearch()),
                Optional.ofNullable(filterUserDto.getSort() == null ? null : UserSortOption.valueOfPreview(filterUserDto.getSort()))
        );
        model.addAttribute("users", filter);
        model.addAttribute("filterUserDto", filterUserDto);
        return "users";
    }


    @GetMapping("{id}")
    public String userInfo(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            User wantedUser = userService.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("wantedUser", wantedUser);
            return "user-info";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }
    }

    @PostMapping("/{id}/promote")
    public String promoteUser(@PathVariable int id,
                              HttpSession session,
                              Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            User userToBePromoted = userService.getById(id);
            userService.promote(id, userToBePromoted);
            userToBePromoted = userService.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("wantedUser", userToBePromoted);
            return "redirect:/users/{id}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @GetMapping("/update/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            User wantedUser = userService.getById(id);

            model.addAttribute("user", wantedUser);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        }

    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserDto dto, @RequestParam("image") MultipartFile multipartFile,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "user";
        }

        try {
            String link = ImageUploadHelper.uploadToCloudinary(multipartFile);
            dto.setAvatar(link);
            User userToBeUpdated = modelMapper.fromDto(dto, id);
            userService.update(id, userToBeUpdated, user);
            return "redirect:/dashboard";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-authorized";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/dashboard";
    }


}
