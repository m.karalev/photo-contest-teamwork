package com.example.photo_contest.controllers.rest;

import com.example.photo_contest.controllers.AuthenticationHelper;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.helpers.ImageUploadHelper;
import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.dto.ReviewDto;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.PhotoService;
import com.example.photo_contest.services.contracts.SubmissionService;
import com.example.photo_contest.services.contracts.UserService;
import com.example.photo_contest.utils_mappers.ModelMapper;
import com.example.photo_contest.utils_mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/photos")
public class PhotoController {

    private final PhotoService photoService;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final ContestService contestService;
    private final SubmissionMapper submissionMapper;
    private final SubmissionService submissionService;

    @Autowired
    public PhotoController(PhotoService photoService, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, UserService userService, ContestService contestService, SubmissionMapper submissionMapper, SubmissionService submissionService) {
        this.photoService = photoService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.contestService = contestService;
        this.submissionMapper = submissionMapper;
        this.submissionService = submissionService;
    }

    @PostMapping
    public Photo upload(@RequestHeader HttpHeaders headers,
                         @RequestParam MultipartFile multipartFile,
                         @RequestParam String title,
                         @RequestParam String story,
                         @RequestParam Integer contestId) {
        Photo photo=new Photo();
        try {
            User user = authenticationHelper.tryGetUser(headers);
            String link = ImageUploadHelper.uploadToCloudinary(multipartFile);
            photo = modelMapper.dtoToObject(title, story, link, user.getId(), contestId);
            photoService.create(photo);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return photo;
    }


    @GetMapping
    public List<Photo> getAll() {
        return photoService.getAll();
    }

    @GetMapping("/{id}")
    public Photo getById(@PathVariable int id) {
        try {
            return photoService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/rate")
    public Submission ratePhoto(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @RequestParam int points,
                          @RequestParam String comment){
        try {
            ReviewDto review = new ReviewDto();
            review.setPoints(points);
            review.setComment(comment);
            Photo photo = photoService.getById(id);
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(photo.getContest().getId());
            Submission submission = submissionMapper.DtoToObject(review, user, photo, contest);
            submissionService.update(submission , user);
            return submission;
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @GetMapping("/filter")
    public List<Photo> filter(@RequestHeader HttpHeaders headers,
                              @RequestParam(required = false) Optional<String> title,
                              @RequestParam(required = false) Optional<String> sort) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return photoService.filter(title, sort);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers,
                         @PathVariable int id){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            photoService.delete(id);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return "Successful deleted.";
    }
}

