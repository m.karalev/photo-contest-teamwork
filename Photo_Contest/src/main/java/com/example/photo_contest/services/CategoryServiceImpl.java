package com.example.photo_contest.services;

import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.Category;
import com.example.photo_contest.repositories.contracts.CategoryRepository;
import com.example.photo_contest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> getAll() {
        return repository.getAll();
    }

    @Override
    public Category getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Category category) {
        boolean duplicateExists = true;
        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        repository.create(category);
    }

    @Override
    public void update(Category category) {
        repository.update(category);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }
}
