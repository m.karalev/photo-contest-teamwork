package com.example.photo_contest.services;

import com.example.photo_contest.email.EmailSender;
import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Role;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.ContestRepository;
import com.example.photo_contest.repositories.contracts.UserRepository;
import com.example.photo_contest.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository repository;
    private final UserRepository userRepository;
    private final EmailSender emailSender;


    @Autowired
    public ContestServiceImpl(ContestRepository repository, UserRepository userRepository, EmailSender emailSender) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.emailSender = emailSender;
    }

    @Override
    public List<Contest> getAll(User user) {
        boolean isOrganizer = isOrganizer(user);
        if (!isOrganizer) {
            throw new UnauthorizedOperationException("You do not have rights!");
        }
        return repository.getAll();
    }

    @Override
    public List<Contest> getPhaseOne(User user) {
        boolean isOrganizer = isOrganizer(user);
        if (!isOrganizer) {
            throw new UnauthorizedOperationException("You do not have rights!");
        }
        return repository.getPhaseOne();
    }

    @Override
    public List<Contest> getPhaseTwo(User user) {
        boolean isOrganizer = isOrganizer(user);
        if (!isOrganizer) {
            throw new UnauthorizedOperationException("You do not have rights!");
        }
        return repository.getPhaseTwo();
    }

    @Override
    public List<Contest> getFinished(User user) {
        boolean isOrganizer = isOrganizer(user);
        if (!isOrganizer) {
            throw new UnauthorizedOperationException("You do not have rights!");
        }
        return repository.getFinished();
    }

    @Override
    public List<Contest> getInvited(User user) {
        return repository.getInvited(user.getId());
    }

    private boolean isOrganizer(User user) {
        Set<Role> roles = user.getRoles();
        boolean isOrg = false;
        for (Role role : roles) {
            if (role.getName().equals("Organizer")) {
                isOrg = true;
            }
        }
        return isOrg;
    }

    @Override
    public Contest getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Contest contest, int[] invitedParticipants, int[] invitedJury) {
        boolean duplicateExists = true;
        try {
            repository.getByName(contest.getTitle());
        } catch (
                EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        repository.create(contest);
        List<User> organizers = userRepository.getOrganizers();
        Contest createdContest = repository.getByName(contest.getTitle());
        for (User user : organizers) {
            createdContest.getJury().add(user);
        }
        for (int userId : invitedJury) {
            createdContest.getJury().add(userRepository.getById(userId));
        }

        for (int userId : invitedParticipants) {

            createdContest.getInvitedParticipants().add(userRepository.getById(userId));
        }
        sendEmails(invitedParticipants, invitedJury);
        repository.update(createdContest);
    }

    private void sendEmails(int[] invitedParticipants, int[] invitedJury) {
        Set<String> participantsEmails = new HashSet<>();
        Set<String> juryEmails = new HashSet<>();
        for (int id : invitedJury) {
            String email = userRepository.getById(id).getEmail();
            juryEmails.add(email);
        }
        for (int id : invitedParticipants) {
            String email = userRepository.getById(id).getEmail();
            if (!juryEmails.contains(email)) {
                participantsEmails.add(email);
            }
        }
        String subjectParticipant = "Invitation to participate in contest";
        String emailParticipant = "Hi dear photographer, you are invited to take place in our new contest.";

        String subjectJury = "Invitation to be jury";
        String emailJury = "Hi dear photographer, yoa are invited to be a jury on our new contest.";
        for (String email : participantsEmails) {
            emailSender.sent(email, emailParticipant, subjectParticipant);
        }
        for (String email : juryEmails) {
            emailSender.sent(email, emailJury, subjectJury);
        }
    }


    @Override
    public void update(Contest contest) {
        boolean duplicateExists = true;
        try {
            Contest existingContest = repository.getByName(contest.getTitle());
            if (existingContest.getId() == contest.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        repository.update(contest);
    }

    @Override
    public List<Contest> getLatest() {
        List<Contest> all = repository.getAll();
        all = all.stream()
                .sorted((c, c1) -> c.getPhaseTwoEnd().compareTo(c1.getPhaseTwoEnd()))
                .limit(4)
                .collect(Collectors.toList());
        return all;
    }

    @Override
    public List<Contest> getActiveOpen() {
        return repository.getActiveOpen();
    }

    @Override
    public List<Contest> getFinishedOpen() {
        return repository.getFinishedOpen();
    }

    @Override
    public List<Contest> getCurrent(int userId) {
        return repository.getCurrent(userId);
    }

    @Override
    public List<Contest> getPast(int userId) {
        return repository.getPast(userId);
    }

    @Override
    public List<Contest> getInvitationsPhaseOne(User user) {
        List<Contest> contests = repository.getPhaseOne();
        List<Contest> invitations = new ArrayList<>();
        for (Contest contest : contests) {
            if (contest.getJury().contains(user)) {
                invitations.add(contest);
            }
        }
        return invitations;
    }

    @Override
    public List<Contest> getInvitationsPhaseTwo(User user) {
        List<Contest> contests = repository.getPhaseTwo();
        List<Contest> invitations = new ArrayList<>();
        for (Contest contest : contests) {
            if (contest.getJury().contains(user)) {
                invitations.add(contest);
            }
        }
        return invitations;
    }


    @Override
    public List<Contest> getFinishedUnCalculated() {
        return repository.getFinishedUnCalculated();
    }

    @Override
    public List<Contest> filter(User user, Optional<String> title, Optional<Integer> categoryId, Optional<String> phase, Optional<String> sort) {
        if (!user.isOrganizer()) {
            throw new UnauthorizedOperationException("Unauthorized operation");

        }
        return repository.filter(title, categoryId, phase, sort);
    }
}
