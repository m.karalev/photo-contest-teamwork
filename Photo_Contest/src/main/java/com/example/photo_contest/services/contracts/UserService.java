package com.example.photo_contest.services.contracts;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.enums.UserSortOption;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll(User user, Optional<String> search);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    void create(User user);

    void update(int id, User user, User user1);

    int getUsersCount();

    List<User> filter(User user, Optional<String> firstName, Optional<String> lastName, Optional<String> username, Optional<String> email, Optional<String> sort);

    List<User> filter(User user, Optional<String> search, Optional<UserSortOption> sort);

    void delete(int id, User user);

    void promote(int id, User userToBePromoted);

    List<User> getOrganizers();

    List<User> getJunkies();

    //  List<Photo> getLatestPhotosForUser(int id);
}
