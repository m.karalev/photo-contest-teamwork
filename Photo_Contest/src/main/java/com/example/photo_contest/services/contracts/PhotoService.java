package com.example.photo_contest.services.contracts;

import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface PhotoService {

  //  void createPhoto(MultipartFile multipartFile, User user) throws IOException;

    void create(Photo photo);

    List<Photo> getAll();

    Photo getById(int id);

    void delete( int id);

    List<Photo> filter(Optional<String> search, Optional<String> sort );

    List<Photo> geAllFromContest(int id);
}
