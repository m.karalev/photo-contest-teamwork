package com.example.photo_contest.services;

import com.example.photo_contest.exceptions.DuplicateEntityException;
import com.example.photo_contest.exceptions.UnauthorizedOperationException;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.repositories.contracts.SubmissionRepository;
import com.example.photo_contest.services.contracts.SubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    private final SubmissionRepository submissionRepository;
    private final PhotoRepository photoRepository;

    @Autowired
    public SubmissionServiceImpl(SubmissionRepository submissionRepository, PhotoRepository photoRepository) {
        this.submissionRepository = submissionRepository;

        this.photoRepository = photoRepository;
    }

    @Override
    public void create(Submission submission) {
        submissionRepository.create(submission);
    }

    @Override
    public List<Submission> getAll(User user) {
        return submissionRepository.getAll();
    }

    @Override
    public Submission getById(int id) {
        return submissionRepository.getById(id);
    }

    @Override
    public void update(Submission submission, User user) {
        Submission forUpdate = getByUserIdPhotoId(submission.getUser().getId(), submission.getPhoto().getId());
        if (!submission.getContest().getJury().contains(user)){
            throw new UnauthorizedOperationException("You are not jury!");
        }else if ( forUpdate.isVoted()){
            throw new DuplicateEntityException("You have already voted!");
        } else {
            forUpdate.setScore(submission.getScore());
            forUpdate.setComment(submission.getComment());
            forUpdate.setVoted(true);
            photoUpdate(submission);
            submissionRepository.update(forUpdate);
        }

    }

    private void photoUpdate(Submission submission) {
        Photo photo = submission.getPhoto();
        photo.setScore(photo.getScore() - 3);
        photo.setScore(photo.getScore() + submission.getScore());
       photoRepository.update(photo);
    }

    @Override
    public Submission getByUserIdPhotoId(int userId, int photoId) {
        return submissionRepository.getByUserIdPhotoId(userId,photoId);
    }

    @Override
    public List<Submission> getByPhotoId(int photoId) {
        return submissionRepository.getByPhotoId(photoId);
    }

}
