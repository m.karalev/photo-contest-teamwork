package com.example.photo_contest.services;


import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.PhotoService;
import com.example.photo_contest.services.contracts.SubmissionService;
import com.example.photo_contest.utils_mappers.SubmissionMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private final SubmissionMapper submissionMapper;
    private final ContestService contestService;
    private final SubmissionService submissionService;



    public PhotoServiceImpl(PhotoRepository photoRepository, SubmissionMapper submissionMapper, ContestService contestService, SubmissionService submissionService) {
        this.photoRepository = photoRepository;
        this.submissionMapper = submissionMapper;
        this.contestService = contestService;
        this.submissionService = submissionService;
    }

    @Override
    public void create(Photo photo) {
        photoRepository.create(photo);
    }

    @Override
    public List<Photo> getAll() {
        return photoRepository.getAll();
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public void delete(int id) {
        photoRepository.delete(id);
    }


    @Override
    public List<Photo> geAllFromContest(int id) {
        return photoRepository.geAllFromContest(id);
    }

    @Override
    public List<Photo> filter(Optional<String> search, Optional<String> sort) {
        return photoRepository.filter(search, sort);
    }
}
