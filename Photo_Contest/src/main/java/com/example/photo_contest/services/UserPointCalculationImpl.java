package com.example.photo_contest.services;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.repositories.contracts.UserRepository;
import com.example.photo_contest.services.contracts.ContestService;
import com.example.photo_contest.services.contracts.UserPointCalculation;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserPointCalculationImpl implements UserPointCalculation {


    public static final int FIRST_PLACE = 50;
    public static final int SHARED_FIRST_PLACE = 40;
    public static final int SECOND_PLACE = 35;
    public static final int SHARED_SECOND_PLACE = 25;
    public static final int THIRD_PLACE = 20;
    public static final int SHARED_THIRD_PLACE = 10;
    public static final int BEING_INVITED_POINTS = 3;
    public static final int JOINING_OPEN_CONTEST = 1;
    public static final int POINTS_FOR_DOUBLE_SCORE_OF_THE_SECOND = 25;

    private final ContestService contestService;

    private final UserRepository userRepository;
    private final PhotoRepository photoRepository;

    public UserPointCalculationImpl(ContestService contestService, UserRepository userRepository, PhotoRepository photoRepository) {
        this.contestService = contestService;
        this.userRepository = userRepository;
        this.photoRepository = photoRepository;
    }

    @Override
    @Scheduled(cron = "0 0 0 * * *")
    @EventListener(ApplicationReadyEvent.class)
    public void pointCalculation() {
        List<Contest> contests = contestService.getFinishedUnCalculated();
        if (!contests.isEmpty()) {

            for (Contest contest : contests) {
                List<Photo> first = new ArrayList<>();
                List<Photo> second = new ArrayList<>();
                List<Photo> third = new ArrayList<>();
                List<Photo> photos = photoRepository.geAllFromContest(contest.getId());
                int counter = 4;
                for (int i = 0; i < photos.size(); i++) {
                    Photo photo = photos.get(i);
                    if (first.size() == 0) {
                        first.add(photo);
                        photo.setRanking(1);
                    } else if (first.get(0).getScore() == photo.getScore()) {
                        first.add(photo);
                        photo.setRanking(1);
                    } else if (second.size() == 0) {
                        second.add(photo);
                        photo.setRanking(2);
                    } else if (second.get(0).getScore() == photo.getScore()) {
                        second.add(photo);
                        photo.setRanking(2);
                    } else if (third.size() == 0) {
                        third.add(photo);
                        photo.setRanking(3);
                    } else if (third.get(0).getScore() == photo.getScore()) {
                        third.add(photo);
                        photo.setRanking(3);
                    } else {
                        photo.setRanking(counter);
                        counter++;
                    }
                    photoRepository.update(photo);
                }
                if (first.size() == 1 && second.size() != 0) {
                    Photo photo1 = first.get(0);
                    Photo photo2 = second.get(0);
                    if (photo1.getScore() / photo2.getScore() >= 2) {
                        User user = photo1.getUser();
                        user.setRanking(user.getRanking() + POINTS_FOR_DOUBLE_SCORE_OF_THE_SECOND);
                        userRepository.update(user);
                    }
                }
                if (first.size() == 1 && second.size() == 0) {
                    Photo photo = first.get(0);
                        User user = photo.getUser();
                        user.setRanking(user.getRanking() + POINTS_FOR_DOUBLE_SCORE_OF_THE_SECOND);
                        userRepository.update(user);
                    }


                for (Photo photo : first) {
                    User user = photo.getUser();
                    if (first.size() == 1) {
                        user.setRanking(user.getRanking() + FIRST_PLACE);
                    } else if (first.size() > 1) {
                        user.setRanking(user.getRanking() + SHARED_FIRST_PLACE);
                    }
                    userRepository.update(user);
                }

                for (Photo photo : second) {
                    User user = photo.getUser();
                    if (second.size() == 1) {
                        user.setRanking(user.getRanking() + SECOND_PLACE);
                    } else if (second.size() > 1) {
                        user.setRanking(user.getRanking() + SHARED_SECOND_PLACE);
                    }
                    userRepository.update(user);
                }

                for (Photo photo : third) {
                    User user = photo.getUser();
                    if (third.size() == 1) {
                        user.setRanking(user.getRanking() + THIRD_PLACE);
                    } else if (third.size() > 1) {
                        user.setRanking(user.getRanking() + SHARED_THIRD_PLACE);
                    }
                    userRepository.update(user);
                }

                for (Photo photo : photos) {
                    User user = photo.getUser();
                    if (photo.isInvited()) {
                        user.setRanking(user.getRanking() + BEING_INVITED_POINTS);
                    } else {
                        user.setRanking(user.getRanking() + JOINING_OPEN_CONTEST);
                    }
                    userRepository.update(user);

                }
                contest.setCalculated(true);
                contestService.update(contest);
            }
        }
    }
}
