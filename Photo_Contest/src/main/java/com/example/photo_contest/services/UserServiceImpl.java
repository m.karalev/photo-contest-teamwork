package com.example.photo_contest.services;


import com.example.photo_contest.exceptions.*;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Role;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.enums.UserSortOption;
import com.example.photo_contest.repositories.contracts.ContestRepository;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import com.example.photo_contest.repositories.contracts.UserRepository;
import com.example.photo_contest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PhotoRepository photoRepository;


    @Autowired
    public UserServiceImpl(UserRepository repository, PhotoRepository photoRepository) {
        this.repository = repository;
        this.photoRepository = photoRepository;
    }

    @Override
    public List<User> getAll(User user, Optional<String> search) {
        return repository.getAll(search);
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public int getUsersCount(){
        return repository.getAll().size();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            repository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateUserException(user.getUsername());
        }
        duplicateExists = true;
        try {
            repository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEmailException(user.getEmail());
        }
        repository.create(user);
    }

    @Override
    public void update(int id, User userToBeUpdated, User user) {
        User existingUser;
        try {
            existingUser = repository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
        if (user.getId() != id) {
            throw new UnauthorizedOperationException("Unauthorized to update other user.");
        }
        repository.update(userToBeUpdated);
        if (!userToBeUpdated.getUsername().equals(existingUser.getUsername())) {
            throw new UnsupportedOperationException("Username can not be updated.");
        }
    }

    @Override
    public List<User> filter(User user, Optional<String> firstName, Optional<String> lastName, Optional<String> username, Optional<String> email, Optional<String> sort) {
        return repository.filter(firstName, lastName, username, email, sort);
    }

    @Override
    public List<User> filter(User user, Optional<String> search, Optional<UserSortOption> sort ) {
        return repository.filter(search, sort);
    }

    @Override
    public void delete(int id, User user) {
        User userToDelete = repository.getById(id);
     if (!user.isOrganizer()){
         throw new UnauthorizedOperationException("Only admin can delete user.");
     }
     repository.delete(userToDelete.getId());
    }

    @Override
    public void promote(int id, User user) {
        User userToPromote = repository.getById(id);
        userToPromote.setOrganizer();
        repository.update(userToPromote);
    }

    @Override
    public List<User> getOrganizers() {
        return repository.getOrganizers();
    }

    @Override
    public List<User> getJunkies() {
        return getAll().stream().filter(user -> !user.isOrganizer()).collect(Collectors.toList());
    }


}
