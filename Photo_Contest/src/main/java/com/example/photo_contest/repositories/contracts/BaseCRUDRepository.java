package com.example.photo_contest.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T>{

    void delete(int id);

    void create(T entity);

    void update(T entity);
}
