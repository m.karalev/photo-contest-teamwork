package com.example.photo_contest.repositories.contracts;

import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;

import java.util.List;

public interface SubmissionRepository extends BaseCRUDRepository<Submission> {

    Submission getByUserIdPhotoId(int userId, int photoId);

     List<Submission>  getByPhotoId( int photoId);
}
