package com.example.photo_contest.repositories.contracts;

import com.example.photo_contest.models.Contest;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    Contest getByName(String title);

    List<Contest> getActiveOpen();

    List<Contest> getFinishedOpen();

    List<Contest> getCurrent(int userId);

    List<Contest> getPast(int userId);

    List<Contest> getPhaseOne();

    List<Contest> getPhaseTwo();

    List<Contest> getFinished();

    List<Contest> getInvited(int userId);

    List<Contest> getFinishedUnCalculated();

    List<Contest> filter(Optional<String> title, Optional<Integer> categoryId ,Optional<String> phase, Optional<String> sort);
}

