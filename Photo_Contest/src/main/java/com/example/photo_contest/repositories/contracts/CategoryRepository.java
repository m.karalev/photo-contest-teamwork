package com.example.photo_contest.repositories.contracts;

import com.example.photo_contest.models.Category;

public interface CategoryRepository extends BaseCRUDRepository<Category> {

   Category getByName(String name);
}
