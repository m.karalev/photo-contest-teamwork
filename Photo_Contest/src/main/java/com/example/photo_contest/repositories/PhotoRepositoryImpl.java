package com.example.photo_contest.repositories;

import com.example.photo_contest.models.Contest;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.PhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PhotoRepositoryImpl extends AbstractCRUDRepository<Photo> implements PhotoRepository {

    @Autowired
    public PhotoRepositoryImpl (SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
    }


    @Override
    public List<Photo> filter(Optional<String> title, Optional<String> sort) {
        if (title.isEmpty() && sort.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Photo");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                filter.add(" title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });
            Query<Photo> queryList = session.createQuery(queryString.toString(), Photo.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);
            return queryList.list();
        }
    }

    @Override
    public List<Photo> geAllFromContest(int id) {
        try (Session session = sessionFactory.openSession()){
            Query<Photo> query = session.createQuery("from  Photo where contest.id = :id order by score desc ");
            query.setParameter("id", id );
            return query.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        queryString.append(" title ");
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }

        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        return queryString.toString();
    }

}
