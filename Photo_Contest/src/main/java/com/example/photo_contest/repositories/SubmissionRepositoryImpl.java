package com.example.photo_contest.repositories;

import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.Photo;
import com.example.photo_contest.models.Submission;
import com.example.photo_contest.models.User;
import com.example.photo_contest.repositories.contracts.SubmissionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubmissionRepositoryImpl extends AbstractCRUDRepository<Submission> implements SubmissionRepository {


    @Autowired
    public SubmissionRepositoryImpl(SessionFactory sessionFactory) {
        super(Submission.class, sessionFactory);
    }

    @Override
    public void create(Submission submission) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(submission);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Submission submission) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(submission);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Submission> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Submission> query = session.createQuery("from Submission", Submission.class);
            return query.list();
        }
    }

    @Override
    public Submission getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Submission submission = session.get(Submission.class, id);
            if (submission == null) {
                throw new EntityNotFoundException("Submission", id);
            }
            return submission;
        }
    }

    @Override
    public Submission getByUserIdPhotoId(int userId, int photoId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Submission> query = session.createQuery("from Submission where user_id = :user_id and photo_id = :photo_id", Submission.class);
            query.setParameter("user_id", userId);
            query.setParameter("photo_id", photoId);
            List<Submission> submissions = query.list();
            Submission submission = submissions.get(0);
            return submission;
        }
    }


    @Override
    public List<Submission>  getByPhotoId( int photoId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Submission> query = session.createQuery("from Submission where photo_id = :photo_id", Submission.class);
            query.setParameter("photo_id", photoId);
            List<Submission> submissions = query.list();
            return submissions;
        }
    }

}