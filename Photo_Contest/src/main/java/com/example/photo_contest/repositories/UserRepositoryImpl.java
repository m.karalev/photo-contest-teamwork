package com.example.photo_contest.repositories;

import com.example.photo_contest.exceptions.EntityNotFoundException;
import com.example.photo_contest.models.User;
import com.example.photo_contest.models.enums.UserSortOption;
import com.example.photo_contest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {


    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }


    @Override
    public List<User> getAll(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();

        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username or email like :email", User.class);
            query.setParameter("firstName", "%" + search.get() + "%");
            query.setParameter("lastName", "%" + search.get() + "%");
            query.setParameter("username", "%" + search.get() + "%");
            query.setParameter("email", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }


    @Override
    public User getByEmail(String email) {
        return getByField("email", email);
    }


    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> username, Optional<String> email, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from User");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            firstName.ifPresent(value -> {
                filter.add(" firstName like :firstName ");
                queryParams.put("firstName", "%" + value + "%");
            });

            lastName.ifPresent(value -> {
                filter.add(" lastName like :lastName ");
                queryParams.put("lastName", "%" + value + "%");
            });

            username.ifPresent(value -> {
                filter.add(" username like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            email.ifPresent(value -> {
                filter.add(" email like :email ");
                queryParams.put("email", "%" + value + "%");
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);
            return queryList.list();
        }
    }

    @Override
    public List<User> filter(Optional<String> search, Optional<UserSortOption> sort) {
        if (search.isEmpty() && sort.isEmpty()) {
            return getAll();
        }
        if (!search.isEmpty() && sort.isEmpty()) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username", User.class);
                query.setParameter("firstName", "%" + search.get() + "%");
                query.setParameter("lastName", "%" + search.get() + "%");
                query.setParameter("username", "%" + search.get() + "%");
                return query.list();
            }
        } else if (!search.isEmpty() && sort.get().getQuery().equals("order by points ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username order by ranking", User.class);
                query.setParameter("firstName", "%" + search.get() + "%");
                query.setParameter("lastName", "%" + search.get() + "%");
                query.setParameter("username", "%" + search.get() + "%");
                return query.list();
            }
        } else if (!search.isEmpty() && sort.get().getQuery().equals("order by points desc ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username order by ranking desc", User.class);
                query.setParameter("firstName", "%" + search.get() + "%");
                query.setParameter("lastName", "%" + search.get() + "%");
                query.setParameter("username", "%" + search.get() + "%");
                return query.list();
            }
        }else if (search.isEmpty() && sort.get().getQuery().equals("order by points ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User order by ranking ", User.class);
                return query.list();
            }
        }else if (search.isEmpty() && sort.get().getQuery().equals("order by points desc ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User order by ranking desc ", User.class);
                return query.list();
            }
        } else if (!search.isEmpty() && sort.get().getQuery().equals("order by date ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username order by date", User.class);
                query.setParameter("firstName", "%" + search.get() + "%");
                query.setParameter("lastName", "%" + search.get() + "%");
                query.setParameter("username", "%" + search.get() + "%");
                return query.list();
            }
        } else if (!search.isEmpty() && sort.get().getQuery().equals("order by date desc ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User where firstName like :firstName or lastName like :lastName or username like :username order by date desc", User.class);
                query.setParameter("firstName", "%" + search.get() + "%");
                query.setParameter("lastName", "%" + search.get() + "%");
                query.setParameter("username", "%" + search.get() + "%");
                return query.list();
            }
        }else if (search.isEmpty() && sort.get().getQuery().equals("order by date ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User order by date ", User.class);
                return query.list();
            }
        }else if (search.isEmpty() && sort.get().getQuery().equals("order by date desc ")) {
            try (Session session = sessionFactory.openSession()) {
                Query<User> query = session.createQuery("from User order by date desc ", User.class);
                return query.list();
            }
        }
        return getAll();
    }

            @Override
            public List<User> getOrganizers () {
                try (Session session = sessionFactory.openSession()) {
                    NativeQuery<User> query = session.createNativeQuery(
                            "select * " +
                                    "from user " +
                                    "join users_roles ur on user.user_id = ur.user_id " +
                                    "where role_id =1 ");
                    query.addEntity(User.class);
                    return query.list();
                }
            }


            private String generateSortString (String value){
                var queryString = new StringBuilder(" order by");
                String[] params = value.split("_");

                if (value.isEmpty()) {
                    throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
                }
                switch (params[0]) {
                    case "firstName":
                        queryString.append(" firstName ");
                        break;
                    case "lastName":
                        queryString.append(" lastName ");
                        break;
                    case "username":
                        queryString.append(" username ");
                        break;
                    case "email":
                        queryString.append(" email ");
                        break;

                }
                if (params.length > 1 && params[1].equals("desc")) {
                    queryString.append(" desc ");
                }

                if (params.length > 2) {
                    throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
                }
                return queryString.toString();
            }

        }

