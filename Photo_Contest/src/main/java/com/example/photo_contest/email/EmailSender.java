package com.example.photo_contest.email;

public interface EmailSender {
    void sent(String to, String email, String subject);
}
