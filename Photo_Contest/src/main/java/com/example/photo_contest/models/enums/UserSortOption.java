package com.example.photo_contest.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOption {

    DATE_ASC("Date, ascending", "order by date "),
    DATE_DESC("Date, descending", "order by date desc "),
    POSTS_ASC("Points, ascending", "order by points "),
    POSTS_DESC("Points, descending", "order by points desc ");

    private final String preview;
    private final String query;

    private static final Map<String, UserSortOption> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOption option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    UserSortOption(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOption valueOfPreview(String preview){
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}
