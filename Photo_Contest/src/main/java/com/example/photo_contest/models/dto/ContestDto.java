package com.example.photo_contest.models.dto;

import com.example.photo_contest.models.Category;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Positive;
import java.util.Date;

public class ContestDto {

    private String title;

    private String description;

    private String coverPhoto;

    private String phaseOneStart;

    private String phaseOneEnd;

    private String phaseTwoStart;

    private String phaseTwoEnd;

    private int categoryId;

    private int[] users;
    private int[] jury;

    private String isOpen;

    public ContestDto() {
    }

    public int[] getJury() {
        return jury;
    }

    public void setJury(int[] jury) {
        this.jury = jury;
    }

    public int[] getUsers() {
        return users;
    }

    public void setUsers(int[] users) {
        this.users = users;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getPhaseOneStart() {
        return phaseOneStart;
    }

    public void setPhaseOneStart(String phaseOneStart) {
        this.phaseOneStart = phaseOneStart;
    }

    public String getPhaseOneEnd() {
        return phaseOneEnd;
    }

    public void setPhaseOneEnd(String phaseOneEnd) {
        this.phaseOneEnd = phaseOneEnd;
    }

    public String getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(String phaseTwoStart) {
        this.phaseTwoStart = phaseTwoStart;
    }

    public String getPhaseTwoEnd() {
        return phaseTwoEnd;
    }

    public void setPhaseTwoEnd(String phaseTwoEnd) {
        this.phaseTwoEnd = phaseTwoEnd;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }
}
