package com.example.photo_contest.models.dto;

public class FilterUserDto {

    private String search;

    private String sort;

    public FilterUserDto() {
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
